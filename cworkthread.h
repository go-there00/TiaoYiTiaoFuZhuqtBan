﻿#ifndef CWORKTHREAD_H
#define CWORKTHREAD_H
#include <QThread>
#include <QDialog>


class CWorkThread : public QThread
{
    Q_OBJECT
public:
    CWorkThread(QDialog *parent = 0);

public slots:
        void start();
        void stop();

    // QThread interface
protected:
        bool oneStep();
        int getPressTime(double iDistance);
        void run();
private:
        bool m_isRun;
};

#endif // CWORKTHREAD_H
